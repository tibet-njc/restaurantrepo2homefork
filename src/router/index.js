import Vue from "vue";
import VueRouter from "vue-router";
import RestaurantCreate from "../views/RestaurantCreate.vue";
import RestaurantList from "../views/RestaurantList.vue";
import RestaurantShow from "../views/RestaurantShow.vue";

Vue.use(VueRouter);



const routes = [

  {
    path: "/",
    name: "restaurant-list",
    component: RestaurantList
  },

  {
    path: "/event/:id",
    name: "restaurant-show",
    component: RestaurantShow,
    props: true
  },

  {
    path: "/event/create",
    name: "restaurant-create",
    component: RestaurantCreate
    
  },
];

const router = new VueRouter({

  mode: 'history',
  
  routes,
});

export default router;
